package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"reflect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Event struct {
	events.CloudwatchLogsEvent
}

func CloudwatchEventHandler(ctx context.Context, event events.CloudwatchLogsEvent) (interface{}, error) {
	data, err := event.AWSLogs.Parse()
	if err != nil {
		return nil, err
	}

	if err := cloudwatchHandler(data); err != nil {
		return nil, err
	}
	return nil, nil
}

func Handler(ctx context.Context, event Event) (interface{}, error) {
	switch {
	case !reflect.DeepEqual(event.CloudwatchLogsEvent, events.CloudWatchEvent{}):
		log.Println("EXECUTING_HANDLER_INFO", "CLOUDWATCH_LOG_EVENT", event.CloudwatchLogsEvent)
		return CloudwatchEventHandler(ctx, event.CloudwatchLogsEvent)
	default:
		return nil, fmt.Errorf("unknown event: %v", event)
	}
}

func main() {
	if os.Getenv("LAMBDA_TASK_ROOT") == "" {
		bot.Debug = false
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates := bot.GetUpdatesChan(u)

		for update := range updates {
			processUpdate(update)
		}
	} else {
		lambda.Start(Handler)
	}
}
