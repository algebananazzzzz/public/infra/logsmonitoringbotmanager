module gitlab.com/algebananazzzzz/testpipeline/main

go 1.21.6

require (
	github.com/aws/aws-lambda-go v1.46.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
)
