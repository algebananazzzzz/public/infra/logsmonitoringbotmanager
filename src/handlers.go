package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/events"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func cloudwatchHandler(data events.CloudwatchLogsData) error {
	messageType := matchFilterType(data.SubscriptionFilters)
	for _, log := range data.LogEvents {
		message := generateLogMessage(messageType, data.SubscriptionFilters, data.LogGroup, log)
		msg := tgbotapi.NewMessage(CHAT_ID, message)
		msg.ParseMode = tgbotapi.ModeHTML
		if _, err := bot.Send(msg); err != nil {
			return err
		}
	}
	return nil
}

func defaultHandler(update tgbotapi.Update) error {
	var message string
	switch update.Message.Command() {
	case "start":
		message = startMessage
	case "get_chatid":
		message = fmt.Sprintln("Your chat id is:\n", update.FromChat().ID)
	case "":
		return nil
	default:
		message = unknownCommandMessage
	}

	msg := tgbotapi.NewMessage(update.FromChat().ID, message)
	msg.ParseMode = tgbotapi.ModeHTML
	if _, err := bot.Send(msg); err != nil {
		return err
	}
	return nil
}

func processUpdate(update tgbotapi.Update) error {
	if update.Message != nil {
		if err := defaultHandler(update); err != nil {
			bot.Send(tgbotapi.NewMessage(update.FromChat().ID, errorMessage))
			return err
		}
	} else {
		updateJson, _ := json.Marshal(update)
		log.Println("PROCESS_UPDATE_WARNING", string(updateJson))
	}
	return nil
}
