package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
)

func matchFilterType(input []string) string {
	highestPriority := INFO_FILTER_TYPE

	// Create a map to store the priority of each filter type
	priorityMap := make(map[string]int)
	for idx, filterType := range filterTypesPriority {
		priorityMap[filterType] = idx
	}

	for _, str := range input {
		for _, filterType := range filterTypesPriority {
			// Check if the string contains the filter type
			if strings.Contains(strings.ToUpper(str), filterType) {
				// Update highestPriority if current filter type has higher priority
				if priorityMap[filterType] > priorityMap[highestPriority] {
					highestPriority = filterType
				}
			}
		}
	}

	return highestPriority
}

func generateLogMessage(logType string, subscriptionFilters []string, logGroup string, log events.CloudwatchLogsLogEvent) string {
	message := fmt.Sprintf(`%v
<b>%v LOG ALERT</b>
An %v log is captured by a Cloudwatch Log Subscription.

<b>Filters:</b>
%v

<b>Timestamp:</b>
%v

<b>Log group:</b>
%v

<b>Log event:</b>
%v`,
		strings.Repeat(messageEmojiMap[logType], 12),
		logType,
		logType,
		strings.Join(subscriptionFilters, ", "),
		time.UnixMilli(log.Timestamp).In(loc).Format("Jan 02 2006, 15:04:05"),
		strings.TrimPrefix(logGroup, "/"),
		log.Message,
	)
	return message
}
