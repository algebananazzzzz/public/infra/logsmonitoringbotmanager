package main

import (
	"log"
	"os"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	startMessage = `🌟 <b>Alarm Monitoring Bot</b> 🌟
Hey there! We're thrilled to have you in our frisbee-loving family. 🥏 Whether you're a seasoned Ultimate player or new to the game, Oasis Ultimate is a place where you can grow, build connections, and make a difference! Let's dive into the world of high-flying, disc-tossing adventures together! 🚀

👉 Type /help for a guide on how to navigate this bot! 🌈🥏😄`
	unknownCommandMessage = ""
	errorMessage          = ""
)

const (
	INFO_FILTER_TYPE     = "INFO"
	DEBUG_FILTER_TYPE    = "DEBUG"
	SUCCESS_FILTER_TYPE  = "SUCCESS"
	WARNING_FILTER_TYPE  = "WARNING"
	ERROR_FILTER_TYPE    = "ERROR"
	CRITICAL_FILTER_TYPE = "CRITICAL"
)

var filterTypesPriority = []string{
	INFO_FILTER_TYPE,
	DEBUG_FILTER_TYPE,
	SUCCESS_FILTER_TYPE,
	WARNING_FILTER_TYPE,
	ERROR_FILTER_TYPE,
	CRITICAL_FILTER_TYPE,
}

var messageEmojiMap = map[string]string{
	INFO_FILTER_TYPE:     "🌟",
	DEBUG_FILTER_TYPE:    "🧪",
	SUCCESS_FILTER_TYPE:  "🎉",
	WARNING_FILTER_TYPE:  "⚠️",
	ERROR_FILTER_TYPE:    "🚨",
	CRITICAL_FILTER_TYPE: "❗",
}

var bot *tgbotapi.BotAPI
var loc *time.Location
var CHAT_ID int64

func init() {
	var err error
	// Load chat id
	CHAT_ID, err = strconv.ParseInt(os.Getenv("CHAT_ID"), 0, 64)
	if err != nil {
		log.Fatalln("INIT_ERROR", "Failed to read CHAT_ID", err.Error())
	}

	loc, _ = time.LoadLocation("Asia/Singapore")
	// Load telegram bot
	bot, err = tgbotapi.NewBotAPI(os.Getenv("TOKEN"))
	if err != nil {
		log.Fatalln("INIT_ERROR", "Failed to initialize telegram bot", err.Error())
	}

	log.Println("INIT_SUCCESS", "Authorized on account", bot.Self.UserName)
}
