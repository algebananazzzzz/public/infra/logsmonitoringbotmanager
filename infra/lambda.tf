locals {
  execution_role_name = "${var.env}-mgmt-iamrole-${var.project_code}"
  function_name       = "${var.env}-app-func-${var.project_code}"
}


module "function_execution_role" {
  source = "./modules/iam_role"
  name   = local.execution_role_name
  policy_attachments = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
}

resource "aws_lambda_permission" "allow_cloudwatch_logs" {
  statement_id  = "AllowExecutionFromCloudWatchLogs"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_function.function.function_name
  principal     = "logs.amazonaws.com"
}

module "lambda_function" {
  source                            = "./modules/lambda_function"
  function_name                     = local.function_name
  ignore_deployment_package_changes = true

  execution_role_arn = module.function_execution_role.role.arn
  deployment_package = {
    filename         = data.archive_file.deployment_package.output_path
    source_code_hash = data.archive_file.deployment_package.output_base64sha256
  }
  runtime = "provided.al2"
  handler = "bootstrap"
  environment_variables = {
    TOKEN   = data.aws_ssm_parameter.token.value
    CHAT_ID = data.aws_ssm_parameter.chat_id.value
  }
}

data "archive_file" "deployment_package" {
  type        = "zip"
  source_dir  = "${path.module}/${var.deployment_package_location}"
  output_path = "${path.module}/../deploy/${var.project_code}.zip"
  excludes    = ["${path.module}/${var.deployment_package_location}/.gitignore"]
}
