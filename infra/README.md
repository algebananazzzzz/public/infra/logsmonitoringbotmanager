## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.4.2 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~>5.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_function_execution_role"></a> [function\_execution\_role](#module\_function\_execution\_role) | ./modules/iam_role | n/a |
| <a name="module_lambda_function"></a> [lambda\_function](#module\_lambda\_function) | ./modules/lambda_function | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_subscription_filter.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_subscription_filter) | resource |
| [aws_lambda_permission.allow_cloudwatch_logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [archive_file.deployment_package](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_ssm_parameter.chat_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.token](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | The AWS region where resources will be deployed. | `string` | `"ap-southeast-1"` | no |
| <a name="input_deployment_package_location"></a> [deployment\_package\_location](#input\_deployment\_package\_location) | The location of the deployment package containing the Lambda code. | `any` | n/a | yes |
| <a name="input_env"></a> [env](#input\_env) | The target environment to which the resources will be deployed. | `any` | n/a | yes |
| <a name="input_log_subscription_filters"></a> [log\_subscription\_filters](#input\_log\_subscription\_filters) | The map of CloudWatch log names and corresponding list of filter patterns for creating log subscription filters. | `map(list(string))` | `{}` | no |
| <a name="input_project_code"></a> [project\_code](#input\_project\_code) | The code name of the project used for naming convention. | `any` | n/a | yes |

## Outputs

No outputs.
