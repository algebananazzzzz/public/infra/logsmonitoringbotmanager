locals {
  log_subscription_filters = flatten([
    for key, value in var.log_subscription_filters :
    [
      for pattern in value :
      "${pattern}#${key}"
    ]
  ])
}


resource "aws_cloudwatch_log_subscription_filter" "this" {
  for_each        = toset(local.log_subscription_filters)
  name            = split("#", each.value)[0]
  log_group_name  = split("#", each.value)[1]
  filter_pattern  = split("#", each.value)[0]
  destination_arn = module.lambda_function.function.arn
}
