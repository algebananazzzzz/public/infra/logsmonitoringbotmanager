variable "aws_region" {
  description = "The AWS region where resources will be deployed."
  default     = "ap-southeast-1"
}

variable "project_code" {
  description = "The code name of the project used for naming convention."
}

variable "env" {
  description = "The target environment to which the resources will be deployed."
}

variable "deployment_package_location" {
  description = "The location of the deployment package containing the Lambda code."
}

variable "log_subscription_filters" {
  description = "The map of CloudWatch log names and corresponding list of filter patterns for creating log subscription filters."
  type        = map(list(string))
  default     = {}

  validation {
    condition = alltrue([
      for key, value in var.log_subscription_filters :
      length(value) <= 2
    ])
    error_message = "Only 2 log group-level subscription filters are allowed."
  }
}
