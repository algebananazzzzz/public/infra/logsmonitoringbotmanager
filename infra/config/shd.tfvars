env                         = "shd"
project_code                = "telenotifbot"
deployment_package_location = "../build"
log_subscription_filters = {
  "/aws/lambda/dev-app-func-oasisultimatebot" = ["DEBUG", "ERROR"]
  "/aws/lambda/dev-app-func-snailbot"         = ["DEBUG", "ERROR"]
  "/aws/lambda/prd-app-func-damnupzbot"       = ["DEBUG", "ERROR"]
}
