data "aws_ssm_parameter" "token" {
  name = "/${var.env}/app/${var.project_code}/TOKEN"
}

data "aws_ssm_parameter" "chat_id" {
  name = "/${var.env}/app/${var.project_code}/CHAT_ID"
}
